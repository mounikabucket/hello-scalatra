package com.qa

import org.scalatra.ScalatraServlet
import org.scalatra.scalate.ScalateSupport

//File Edited by mounika
//2 fnd 1 ile has been edited 3 mon ago

// test
class MainServlet extends ScalatraServlet with ScalateSupport {
  before() {
    contentType = "text/html"
  }
//this is for testing

  get("/") {
    layoutTemplate("/WEB-INF/templates/views/index.ssp")
  }

  get("/dinosaur") {
    layoutTemplate("/WEB-INF/templates/views/dinosaur.ssp")
  }
}
